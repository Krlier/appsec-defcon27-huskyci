# AppSec-defcon27-huskyCI

> This repository was used as a huskyCI POC at Defcon 27 AppSec Village

* [AppSec Village](https://www.appsecvillage.com/)

<img src="images/appSecVillage.png" align="center" height="100" />

* [huskyCI](https://github.com/globocom/huskyCI)

<img src="images/huskyCI-logo.png" align="center" height="" />

## Requirements

#### Golang

You must have [Go](https://golang.org/doc/install) installed and this repo needs to be inside your [$GOPATH](https://github.com/golang/go/wiki/GOPATH) to run properly.

Mac OS:
```sh
brew install go
```

Linux:
```sh
sudo apt-get install golang-go
```
## Running

```sh
make run
```

```sh
⇨ http server started on [::]:8888
```

##
